import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import GUI from "lil-gui";

/**
 * Base
 */
const gui = new GUI({ width: 300 }); // Crée une interface graphique pour le débogage
const canvas = document.querySelector("canvas.webgl"); // Sélectionne le canvas WebGL
const scene = new THREE.Scene(); // Crée une nouvelle scène

/**
 * Galaxy
 */
// Parameters
const parameters = {}; // Stocke les paramètres de la galaxie
parameters.count = 100000; // Nombre de particules dans la galaxie
parameters.size = 0.01; // Taille des particules
parameters.radius = 3; // Rayon de la galaxie
parameters.branches = 6; // Nombre de branches dans la galaxie
parameters.spin = 1; // Angle de rotation des branches de la galaxie
parameters.randomness = 0.5; // Facteur de randomisation des positions des particules
parameters.randomnessPower = 3; // Puissance de la randomisation
parameters.insideColor = "#ff6030"; // Couleur intérieure de la galaxie
parameters.outsideColor = "#1b3984"; // Couleur extérieure de la galaxie

let geometry = null; // Géométrie des particules
let material = null; // Matériau des particules
let points = null; // Ensemble des particules

const generateGalaxy = () => {
  // Destroy old galaxy
  if (points !== null) {
    geometry.dispose(); // Libère la mémoire utilisée par la géométrie précédente
    material.dispose(); // Libère la mémoire utilisée par le matériau précédent
    scene.remove(points); // Supprime les particules de la scène
  }

  // Geometry
  geometry = new THREE.BufferGeometry(); // Crée une nouvelle géométrie
  const positions = new Float32Array(parameters.count * 3); // Tableau des positions des particules
  const colors = new Float32Array(parameters.count * 3); // Tableau des couleurs des particules

  const insideColor = new THREE.Color(parameters.insideColor); // Couleur intérieure convertie en objet THREE.Color
  const outsideColor = new THREE.Color(parameters.outsideColor); // Couleur extérieure convertie en objet THREE.Color

  for (let i = 0; i < parameters.count; i++) {
    const i3 = i * 3;

    // Positions
    const radius = Math.random() * parameters.radius; // Rayon aléatoire pour chaque particule
    const spinAngle = radius * parameters.spin; // Angle de rotation en fonction du rayon
    const branchesAngle =
      ((i % parameters.branches) / parameters.branches) * Math.PI * 2; // Angle de la branche en fonction de l'index de la particule

    const randomX =
      Math.pow(Math.random(), parameters.randomnessPower) *
      (Math.random() < 0.5 ? 1 : -1) *
      parameters.randomness *
      radius; // Position aléatoire en X
    const randomY =
      Math.pow(Math.random(), parameters.randomnessPower) *
      (Math.random() < 0.5 ? 1 : -1) *
      parameters.randomness *
      radius; // Position aléatoire en Y
    const randomZ =
      Math.pow(Math.random(), parameters.randomnessPower) *
      (Math.random() < 0.5 ? 1 : -1) *
      parameters.randomness *
      radius; // Position aléatoire en Z

    positions[i3 + 0] = Math.cos(spinAngle + branchesAngle) * radius + randomX; // Position X de la particule
    positions[i3 + 1] = randomY; // Position Y de la particule
    positions[i3 + 2] = Math.sin(spinAngle + branchesAngle) * radius + randomZ; // Position Z de la particule

    // Colors
    const mixedColor = insideColor.clone(); // Copie de la couleur intérieure
    mixedColor.lerp(outsideColor, radius / parameters.radius); // Mélange de la couleur intérieure et extérieure en fonction du rayon

    colors[i3 + 0] = mixedColor.r; // Composante rouge de la couleur de la particule
    colors[i3 + 1] = mixedColor.g; // Composante verte de la couleur de la particule
    colors[i3 + 2] = mixedColor.b; // Composante bleue de la couleur de la particule
  }

  geometry.setAttribute("position", new THREE.BufferAttribute(positions, 3)); // Ajoute les positions à la géométrie
  geometry.setAttribute("color", new THREE.BufferAttribute(colors, 3)); // Ajoute les couleurs à la géométrie

  // Material
  material = new THREE.PointsMaterial({
    size: parameters.size, // Taille des particules
    sizeAttenuation: true,
    depthWrite: false,
    blending: THREE.AdditiveBlending,
    vertexColors: true, // Utilise les couleurs des vertices
  });

  // Points
  points = new THREE.Points(geometry, material); // Crée un ensemble de particules avec la géométrie et le matériau
  scene.add(points); // Ajoute les particules à la scène
};

generateGalaxy();

gui
  .add(parameters, "count")
  .min(100)
  .max(100000)
  .step(100)
  .onFinishChange(generateGalaxy); // Ajoute un contrôle pour le nombre de particules
gui
  .add(parameters, "size")
  .min(0.001)
  .max(0.1)
  .step(0.001)
  .onFinishChange(generateGalaxy); // Ajoute un contrôle pour la taille des particules
gui
  .add(parameters, "radius")
  .min(0.01)
  .max(20)
  .step(0.01)
  .onFinishChange(generateGalaxy); // Ajoute un contrôle pour le rayon de la galaxie
gui
  .add(parameters, "branches")
  .min(2)
  .max(20)
  .step(1)
  .onFinishChange(generateGalaxy); // Ajoute un contrôle pour le nombre de branches
gui
  .add(parameters, "spin")
  .min(0)
  .max(5)
  .step(0.001)
  .onFinishChange(generateGalaxy); // Ajoute un contrôle pour la vitesse de rotation
gui
  .add(parameters, "randomness")
  .min(0)
  .max(2)
  .step(0.001)
  .onFinishChange(generateGalaxy); // Ajoute un contrôle pour le facteur de randomisation
gui
  .add(parameters, "randomnessPower")
  .min(1)
  .max(10)
  .step(0.001)
  .onFinishChange(generateGalaxy); // Ajoute un contrôle pour la puissance de la randomisation
gui.addColor(parameters, "insideColor").onFinishChange(generateGalaxy); // Ajoute un contrôle pour la couleur intérieure
gui.addColor(parameters, "outsideColor").onFinishChange(generateGalaxy); // Ajoute un contrôle pour la couleur extérieure

/**
 * Sizes
 */
const sizes = {
  width: window.innerWidth,
  height: window.innerHeight,
};

window.addEventListener("resize", () => {
  // Update sizes
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;

  // Update camera
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();

  // Update renderer
  renderer.setSize(sizes.width, sizes.height);
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(
  75,
  sizes.width / sizes.height,
  0.1,
  100
);
camera.position.x = 3;
camera.position.y = 3;
camera.position.z = 3;
scene.add(camera);

// Controls
const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
  canvas: canvas,
});
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

/**
 * Animate
 */
const clock = new THREE.Clock();

const tick = () => {
  const elapsedTime = clock.getElapsedTime();

  points.rotation.y = elapsedTime * 0.1;
  points.rotation.x = Math.sin(elapsedTime) * 0.05;
  points.rotation.z = Math.cos(elapsedTime) * 0.05;

  // Update controls
  controls.update();

  // Render
  renderer.render(scene, camera);

  // Call tick again on the next frame
  window.requestAnimationFrame(tick);
};

tick();
