# Galaxy Generator

> https://galaxy-generator-two-zeta.vercel.app/

## Setup

Télécharge [Node.js](https://nodejs.org/en/download/).
Lance les commandes suivantes:

```bash
# Installe les dépendances (que la première fois)
npm install

# Lance le serveur local sur localhost:5173
npm run dev
```

### Pour les débutants :

Si tu débutes la programmation et que tu as besoin d'un guide plus détaillé, suis ces étapes:

1. Il faut télécharger et installer [Node.js](https://nodejs.org/en/download/) si ce n'est pas déjà fait.
2. Télécharge les fichiers sources du projet (avec git si tu sais t'en servir ou en téléchargeant le [`.zip`](https://gitlab.com/LisaAnthonioz/galaxy-generator/-/archive/main/galaxy-generator-main.zip))
3. Ouvre un terminal là où se trouve le projet
4. Installe les dépendances en tapant la commande suivante :

```bash
npm install
```

5. Une fois que les dépendances sont installées, tu peux lancer le serveur local en lançant la commande suivante, cela va rendre le projet accessible à cette adresse `localhost:5173` sur ton navigateur web.

```bash
npm run dev
```

6. Tu peux ouvrir ton navigateur web préféré et naviguer sur cette adresse `localhost:5173` pour voir le projet en action.

Et voilà ! Tu devrais maintenant être capable lancer le projet sur ton ordinateur.
Si tu veux relancer le projet plus tard, il suffit de recommencer à partir de l'étape 5 !

## Infos importantes

> **Ecoute pendant les cours de trigo, ça permet de faire des trucs cools comme ça !**

Dans ce projet, le but est de générer une galaxy composée de particules en fonction de paramètres variables gérés pas l'utilisateur.
La problématique principale est de calculer la position de chaque particule.

La position d'une particule est composée de 3 valeurs : X, Y, Z.

Tout le calcul des positions des particules pour avoir ce résultat est dans le fichier [`src/script.js`](https://gitlab.com/LisaAnthonioz/galaxy-generator/-/blob/main/src/script.js).

Ce bout de code permet de calculer les positions des particules, en fonction de ces paramètres :

- le rayon: `parameters.radius`,
- l'angle de rotation des branches: `parameters.spin`,
- du nombres de branches: `parameters.branches`,
- du niveau d'aléatoire dans la position des particules: `parameters.randomness` et `parameters.randomnessPower`

`positions[i3 + 0]`, `positions[i3 + 1]` et `positions[i3 + 2]` représente respectivement, le X, le Y et le Z de chaque particule.

```javascript
// Positions
const radius = Math.random() * parameters.radius;
const spinAngle = radius * parameters.spin;
const branchesAngle =
  ((i % parameters.branches) / parameters.branches) * Math.PI * 2;

const randomX =
  Math.pow(Math.random(), parameters.randomnessPower) *
  (Math.random() < 0.5 ? 1 : -1) *
  parameters.randomness *
  radius;
const randomY =
  Math.pow(Math.random(), parameters.randomnessPower) *
  (Math.random() < 0.5 ? 1 : -1) *
  parameters.randomness *
  radius;
const randomZ =
  Math.pow(Math.random(), parameters.randomnessPower) *
  (Math.random() < 0.5 ? 1 : -1) *
  parameters.randomness *
  radius;

positions[i3 + 0] = Math.cos(spinAngle + branchesAngle) * radius + randomX;
positions[i3 + 1] = randomY;
positions[i3 + 2] = Math.sin(spinAngle + branchesAngle) * radius + randomZ;
```

Ce bout de code permet lui de gérer la rotation de la galaxy en fonction du temps, on fait varier la position des particules en fonction du temps passé.

```javascript
const tick = () => {
  const elapsedTime = clock.getElapsedTime();

  points.rotation.y = elapsedTime * 0.1;
  points.rotation.x = Math.sin(elapsedTime) * 0.05;
  points.rotation.z = Math.cos(elapsedTime) * 0.05;
};
```
